#!/bin/ruby

require 'facter'

Facter.add("environment") do 

  setcode do

    hostname = Facter.value(:hostname)
    if hostname.match /(^\w)(\D{2,5})(\d)(\d)(\d+)/
      if hostname.match /^d\w+/
        environment = "dev"
      elsif hostname.match /^l\w+/
        environment = "lab"
      elsif hostname.match /^q\w+/
        environment = "qa"
      elsif hostname.match /^s\w+/
        environment = "preprod"
      elsif hostname.match /^p\w+/
        environment = "production"
      else 
        environment = "production"
      end

    elsif hostname.match /(^\w)(\d\d)(\w)(\d\d)(\w{3})(\w)(\w)(\w)(\d{3})/
      if hostname.match /^d\w+/
        environment = "dev"
      elsif hostname.match /^l\w+/
        environment = "lab"
      elsif hostname.match /^q\w+/
        environment = "qa"
      elsif hostname.match /^p\w+/
        environment = "production"
      elsif hostname.match /^n\w+/
        environment = "nonprod"
      elsif hostname.match /(^s|^u)\w+/
        environment = "preprod"
      elsif hostname.match /^p\w+/
        environment = "production"
      else 
        environment = "production"
      end

    elsif hostname.match /^usg-\w+/
      environment = "production"

    elsif hostname.match /jamison.*/
        environment = "awesome"
    
    else 
      environment = "production"
    end
    
  end
end
