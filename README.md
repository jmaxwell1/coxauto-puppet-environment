# environment

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with environment](#setup)
    * [What environment affects](#what-environment-affects)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Overview

This module adds a custom fact based on the hostname of the system.

## Module Description

We do regex matching to determine the hostname.  The general structure
is that if it matches a give hostname convention, check for what in that
convention determines environment.  

## Setup

None

### What environment affects

Only a custom fact, no system changes.  Currently, UAT and Preproduction are
treated as the same environement.  While this is definitely true for the 
operations side of the house, it might not be for the teams more focused on
applications.  It is very probable that this will change in the future.



## Reference

Like above, double conditional regex statement.  If match looks like foo,
match the peice that determines environment, then set that as the environment
fact.  

## Development

Use vi...

## ChangeLog

2015.11.05 - Jamison - Initial writing, should work...
